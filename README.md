# PRG-FRONTEND #

### What is this repository for? ###

* This is a frontend of Program Repair Golf project.
* version 0.0.3

### How to install ###

 * `$ git clone git@bitbucket.org:edudepetris/prg-frontend.git`
 * `$ cd prg-frontend`
 * See: [ Fix npm permissions ](https://docs.npmjs.com/getting-started/fixing-npm-permissions) if sudo is needed in npm install
 * `$ npm install --global yo gulp-cli bower generator-webapp`
 * `$ bower install`
 * `$ npm install`

### How to run
 * `$ gulp serve`

### How to run test
 * `$ npm test` or `$ yarn test`

### Deployments
 * [Staging](https://programrepairgolfstaging.herokuapp.com)
 * [Production](https://programrepairgolf.herokuapp.com)

### Our Stack & Tools
 * [Scaffold](https://github.com/yeoman/generator-webapp)
 * [Project Management](https://www.pivotaltracker.com/n/projects/2115994)
 * [Hosting service](https://bitbucket.org)
 * [Team communication](https://slack.com/)
 * [Cloud platform](https://www.heroku.com/what)

### Our Workflow
 * [team workflow](http://nvie.com/posts/a-successful-git-branching-model/)
 * [CI](https://bitbucket.org/product/features/pipelines)
