const express = require('express');
const serveStatic = require('serve-static');
const compression = require('compression');
const port = process.env.PORT || 3000;
const domain =  process.env.DOMAIN;

function ensureDomain(req, res, next) {
  if (req.url.length>1 && req.url.indexOf('.')<0 && req.url.indexOf('?')<0) {
    res.writeHead(301, {Location: '/?'+req.url.substring(1)});
    res.end('ok');
  }  

  if (!domain || req.hostname === domain) {
    // OK, continue
    return next();
  };
  // handle port numbers if you need non defaults
  res.redirect(`http://${domain}${req.url}`);
};

const app = express();

// at top of routing calls
app.all('*', ensureDomain);

app.use(compression());

// default to .html (you can omit the extension in the URL)
app.use(serveStatic(`${__dirname}/public`, {'extensions': ['html']}));

app.listen(port, () => {
  console.log('Server running...');
});

