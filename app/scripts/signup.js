import page from 'page'
import api from './config/api'
import * as session from './config/session'

  let url = '/sign_up';

  export default function () {
  	const html = PRG.templates.signup()
  	document.querySelector('#content').innerHTML = html
    let button = $('#submitData');

    button.click(function() {
      var inputEmail = document.getElementById('inputEmail');
      var password1 = document.getElementById('inputPassword').value;
      var password2 = document.getElementById('inputRePassword').value;
      var termsCheckbox = document.getElementById('termsCheckbox');
      var username = document.getElementById('username').value;

      $('#signUpError').removeClass('alert alert-danger').html('');

      if (inputEmail.checkValidity() && password1 == password2 && termsCheckbox.checked) {
        api.post(url, {
          email: inputEmail.value,
          name: username,
          password: password1,
          pass_confirmation: password2
        })
        .then(res => {
          //Can signIn
          return api.post('/sign_in', {
            email: inputEmail.value,
            password: password1
          })
        })
        .then(res => {
          session.signin(res.data,true)
          page.redirect('/')
        })
        .catch(function (error) {
          $('#signUpError').addClass('alert alert-danger no-margin-bottom').html('<strong>Error!</strong> ' + error.response.data.error);
        });
      }
      else {
        if (inputEmail.checkValidity() == false) {
          $('#signUpError').addClass('alert alert-danger no-margin-bottom').html('<strong>Error!</strong> Invalid Email.');
        }
        else if (password1 != password2) {
          $('#signUpError').addClass('alert alert-danger no-margin-bottom').html('<strong>Error!</strong> Invalid Passwords.');
        }
        else if (!termsCheckbox.checked) {
          $('#signUpError').addClass('alert alert-danger no-margin-bottom').html('<strong>Error!</strong> Please accept our terms and conditions.');
        }
      }
    })
   }
