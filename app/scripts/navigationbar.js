import * as session from './config/session'
import api from './config/api'

let actualSearchValue = "";

export default function (ctx,next) {
  let data1 = {};
  let porcen = {};
  if (session.isOpen()) {
    data1 = session.getUserData();
    if(!data1.name) data1.name=data1.username
    const html = PRG.templates.navigationbar(data1);
    document.querySelector('#nav-container').innerHTML = html;
    setactive(ctx.path.substring(1));
    next();

    api.get('/user').then(res => {
      var porc = cal_porc(res.data);

      $('#progressBar').css('width', porc + '%');
      $('#progressBar').html(porc + '% Completed Profile');

      if (res.data.imageURL) {
        $('#avatar').attr('src',res.data.imageURL).css('display','block')
        $('#defavatar').css('display','none')
      }
    })

    $('#search').html('');
    $('#search').select2({
      placeholder: 'Search for Users'
    });

    $('#searchProfile').submit(function(e) {
      e.preventDefault();
      let userToSearch = $('#search').val();
      if (userToSearch != "") {
        location.replace('/profile/'+userToSearch);
      }
    });

    $("#search").on("select2:open", function() {
      $('.select2-results').addClass('hiddenElement')

      $(".select2-search__field").attr("placeholder", "Search for users...");
      var searchUserBar = $(".select2-search__field")[0];
      searchUserBar.oninput = function() {
        var val = searchUserBar.value;
        actualSearchValue = val;
        if ((val.length >= 3) && ($('#search').html() == "")) {
          updateSearchOptions(val);
          $('#searchDiv .select2-dropdown').show();
        }
        else if (val.length < 3 ) {
          $('#searchDiv .select2-dropdown').hide();
          clearSearchOptions();
        }
      }
      $(document).click(function(event) {
        var input = $('.select2-search__field')[0];
        if (event.target == input) {
          expandSearchBar();
        }
        else {
          contractSearchBar();
        }
      })
    });

    $("#search").on("select2:close", function() {
      $(".select2-search__field").attr("placeholder", null);
      $(".select2-search__field").attr("oninput", null);
    });

    $('#search').on('change', function(e) {
      var selected = $('#search').find(':selected');
      if (selected) {
        var selectedName = selected[0].innerHTML;
        var link = document.createElement("a");        // Create a <button> element
        link.href="/profile/"+selectedName;               // Create a text node
        $('#responsiveDiv').append(link);
        link.click();
      }
    });

    $('#searchDiv').hover(function() {
      $('.select2').css("display","block");
      var searchInput = $(".select2-search__field");
      if (!(searchInput.is(':focus'))) {
        searchInput.click();
      }
    });
  }
  else {
    const html = PRG.templates.navigationbar(data1);
    document.querySelector('#nav-container').innerHTML = html;
    setactive(ctx.path.substring(1));
    next();
  }
}

function setactive(path) {
  $('li.active').removeClass('active');
  $('a[href*="'+path+'"]').parent().addClass('active');
}

function clearSearchOptions() {
  $('#search').html('');
}

function updateSearchOptions(str) {
  let userArray = [];
  clearSearchOptions();
  api.get(`/users?substring=${str}`)
  .then( response => {
    var data = response.data;
    $.each(data, function(k, v) {
      userArray.push(v);
    })
    var options = "";
    for (var i = 0; i < userArray.length; i++) {
      $('#search').append('<option id="search_option_'+i+'">'+userArray[i]+'</option>');
      $('#search_option_'+i).click(function(e) {
        e.preventDefault();
      });
    }

    $('.search_option').click(function(e) {
      e.preventDefault();
    });

    $(".select2-search__field").click();
    $(".select2-search__field").click();
    $(".select2-search__field").val(actualSearchValue);
    $('.select2-results').removeClass('hiddenElement');
  });
}

function expandSearchBar() {
  $('.select2').css("display","block");
  $('.search-form .form-group')[0].style.width = '100%';
}

function contractSearchBar() {
  $('.search-form .form-group')[0].style.width = '';
  $('.select2').css("display","none");
  $('#searchDiv .select2-dropdown').hide();
}

function cal_porc(data) {
  var porc = 0;
  if (data.name && data.name != ""){porc ++}
  //if (data.email && data.email != ""){porc ++}
  if (data.country && data.country != ""){porc ++}
  //if (data.joinDate && data.joinDate != ""){porc ++}
  if (data.username && data.username != ""){porc ++}
  if (data.occupation && data.occupation != ""){porc ++}
  if (data.imageURL && data.imageURL != "../images/profile/defaultProfile.jpg"){porc ++}

  porc = ((porc * 100) / 5 );

  return  Math.round(porc);
}
