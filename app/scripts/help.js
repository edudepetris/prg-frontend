
export default function () {
    const html = PRG.templates.help()
    document.querySelector('#content').innerHTML = html

    function changeCollapse(elem) {
    	if (elem.html() == 'View details »') {
    		elem.html('Hide details &raquo')
    	}
    	else {
    		elem.html('View details &raquo')
    	}
    }

    $('#verificationDetailsBtn').click(function() {
    	changeCollapse($('#verificationDetailsBtn'))
    })

    $('#testPassingDetailsBtn').click(function() {
    	changeCollapse($('#testPassingDetailsBtn'))
    })

    $('#compilationDetailsBtn').click(function() {
    	changeCollapse($('#compilationDetailsBtn'))
    })
}

