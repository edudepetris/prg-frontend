import page from 'page'
import api from './config/api'

export default function (username) {

  api.get('/user?'+username)
  .then(res => {
    const html = PRG.templates.profile(res.data)
    document.querySelector('#content').innerHTML = html
    $('#editBtn').remove();
    $('#settingsBtn').remove();
    $('#edit-avatar').remove();
  })
}
