function set_levels_my(jsonData) {
  var levels = ['','Very Easy','Easy','Normal','Hard','Very Hard'];
  jsonData.myChallenges.forEach(function (chall,pos) {
    chall.level=levels[chall.difficulty];
  });
}

function set_type_my(jsonData) {
  var types = ['','Compilation','Test Passing','Verification'];
  jsonData.myChallenges.forEach(function (chall,pos) {
    chall.typen=types[chall.type];
  });
}

export {set_type_my,set_levels_my}