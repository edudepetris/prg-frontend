function set_levels(jsonData) {
  var levels = ['','very easy','easy','normal','hard','very hard'];
  jsonData.challenges.forEach(function (chall,pos) {
    chall.language = chall.language.toLowerCase()
    if (typeof chall.difficulty == "string") {
      let dif = chall.difficulty.toLowerCase().replace('_',' ');
      chall.level= dif;
      chall.difficulty = levels.indexOf(dif)
    }
    else  
      chall.level=levels[chall.difficulty];
    chall.description=breakDescription(chall.description)
  });
}

function set_type(jsonData) {
  var types = ['','Compilation','Test Passing','Verification'];
  jsonData.challenges.forEach(function (chall,pos) {
    if (typeof chall.type == "string") 
      chall.typen=chall.difficulty;
    else  chall.typen=types[chall.type+1];
  });
}

export {set_type,set_levels}

function longestWord(str) {
  const words = str.split(' ');
  return words.reduce((champ, contender) => 
    (contender.length > champ.length) ? contender: champ
  );
}

function breakDescription(desc) {
  while (longestWord(desc).length>40) {
    let lword = longestWord(desc);
    let lword2 = (lword.match(/.{1,40}/g)).join(' ')
    desc = desc.replace(lword,lword2)
  }
  return desc
}