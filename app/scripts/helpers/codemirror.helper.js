/*
    CodeMirror Module
    Usage:
      import codeMirror from './helpers/codemirror.helper'
      
      //create editor instance
      var editor = codeMirror('#{text-area-id}')
      
      //get text content from instance
      editor.getValue()
*/


export default function (textarea_sel) {
  
  var textarea = $(textarea_sel)[0];
  if (!textarea) return null;
  //replace textarea with template
  var templ = PRG.templates.codeEditorTemp({'ta':textarea.outerHTML})
  var aux = document.createElement('div')
  aux.innerHTML=templ;
  textarea.parentElement.replaceChild(aux.firstElementChild, textarea)
  
  textarea = $(textarea_sel)[0];
  var deftheme = (localStorage.deftheme)? localStorage.deftheme : 'monokai';
  var codemirror = CodeMirror.fromTextArea(textarea,{
            lineNumbers: true,
            tabSize: 2,
            matchBrackets: true,
            mode: 'text/x-java',
            theme: deftheme,
            scrollbarStyle: 'overlay',
            autoCloseBrackets: true,
            extraKeys: {
              'F11': function(cm) {
                cm.setOption('fullScreen', !cm.getOption('fullScreen'));
              },
              'Esc': function(cm) {
                if (cm.getOption('fullScreen')) cm.setOption('fullScreen', false);
              },
              Tab: function(cm) {
                var spaces = Array(cm.getOption('indentUnit') + 1).join(' ');
                cm.replaceSelection(spaces);
              }
            }
          });
    //auto indent lines
    var lines = codemirror.lineCount();
    for (var i = 0; i < lines; i++) {
      codemirror.indentLine(i,'smart');
    }
    //Autocomplete keyword when press 'ctrl+space'
    var mac = CodeMirror.keyMap.default == CodeMirror.keyMap.macDefault;
    CodeMirror.keyMap.default[(mac ? 'Cmd' : 'Ctrl') + '-Space'] = 'autocomplete';

    var themeSelector = $('#select-theme');
    //fill selector with options
    get_themes().forEach(function (thme) {
      var opt = document.createElement('option');
      opt.innerText=thme;
      opt.value=thme;
      themeSelector.append(opt);
    })
    $('option[value="'+deftheme+'"]').attr('selected',true);

    //Editor Theme Selection
    themeSelector.on('change',function() {
      var theme = this.value;
      codemirror.setOption('theme', theme);
      localStorage.deftheme=theme;
    });

    $('[data-toggle="tooltip"]').tooltip();

    //Toggle full screen on when press-button
    $('.fsbtn').on('click',function () {
      codemirror.setOption('fullScreen', true);
      //Show button to exit full screen
      $('#fixed-btn').css('display','block');
    });

    $('#fixed-btn').on('click',function () {
      //exit full screen and hide button
      codemirror.setOption('fullScreen', false);
      $(this).css('display','none');
    })

  return codemirror;
}

function get_themes() {
  /*var cmthemes=['default'];
    $('link[href*="/theme"]').each(function(){
      var href = this.href;
      var splits = href.split('/');
      var name = splits[splits.length-1].split('.')[0];
      cmthemes.push(name);
    });*/
  return ['default','3024-day','3024-night','abcdef',
  'ambiance-mobile','ambiance','base16-dark','base16-light','bespin',
  'blackboard','cobalt','colorforth','dracula','duotone-dark',
  'duotone-light','eclipse','elegant','erlang-dark','hopscotch',
  'icecoder','isotope','lesser-dark','liquibyte','material','mbo',
  'mdn-like','midnight','monokai','neat','neo','night','panda-syntax',
  'paraiso-dark','paraiso-light','pastel-on-dark','railscasts','rubyblue',
  'seti','solarized','the-matrix','tomorrow-night-bright',
  'tomorrow-night-eighties','ttcn','twilight','vibrant-ink','xq-dark',
  'xq-light','yeti','zenburn']
}