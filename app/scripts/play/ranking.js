import api from '../config/api'
import codem from '../helpers/codemirror.helper'

export default function (c_id) {
  
  api.get('/challenge/'+c_id+'/ranking')
  .then(res => {
    res.data.ranking.forEach((rank,i) => {
      setTime(rank)
      rank.position=i+1;
    });
    let html = PRG.templates.ranking(res.data)
    $('#ranking-tab').html(html)
    
    if (res.data.ranking === undefined 
        || res.data.ranking.length == 0) {
      $('#noranks-alert')[0].style.display=null;
    }
  })
  .catch(error => {
    if (error.response && error.response.status==404) {
      page.redirect('/notfound');
    }
  })
}


function setTime(data) {
  let date = new Date(data.date);
  let day = date.toLocaleDateString();
  let time = date.toTimeString().split(' ')[0];
  data.day=day;
  data.time=time;
}