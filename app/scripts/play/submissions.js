import api from '../config/api'
import codem from '../helpers/codemirror.helper'

export default function (c_id) {
  
  api.get('/game/'+c_id+'/submissions')
  .then(res => {
    res.data.submissions.forEach(sub => setTime(sub));
    let html = PRG.templates.submissions(res.data)
    $('#subs-tab').html(html)

    if (res.data.submissions === undefined 
        || res.data.submissions.length == 0) {
      $('#empty-alert')[0].style.display=null;
    }
    
    //assign action to view btn
    $('.view-btn').on('click',function(ev) {
      let code = this.parentElement.querySelector('textarea').value;
      $('#subm-code').html(code);
      $('#subm-modal div')[0].style.display='block';
      let cm = codem('#subm-code');
      $('#subm-modal .code-bottom p')[0].style.display='none';
      $('#subm-modal .fsbtn')[0].style.display='none';
      cm.setOption('readOnly',true);
    })
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        let modal=$('#subm-modal div')[0];
        if (event.target == modal) {
            modal.style.display = "none";
            $('.subm-code').html('<textarea id="subm-code"></textarea>');
        }
    }
  })
  .catch(error => {
    if (error.response && error.response.status==404) {
      page.redirect('/notfound');
    }
  })
}


function setTime(data) {
  let date = new Date(data.date);
  let day = date.toLocaleDateString();
  let time = date.toTimeString().split(' ')[0];
  data.day=day;
  data.time=time;
}