import page from 'page'
import api from './config/api'

  export default function () {
    const html = PRG.templates.contact()
    document.querySelector('#content').innerHTML = html
    let button = $('#submit');

    button.click(function(event) {
      event.preventDefault();
      var inputEmail = document.getElementById('email');
      var inputName = document.getElementById('name').value;
      var inputAsunto = document.getElementById('razon').value;
      var inputMessage = document.getElementById('message').value;
      var mail = document.getElementById('email').value;

      $('#contactFormError').removeClass('alert alert-danger').html('');
      $('#contactFormSuccess').removeClass('alert alert-success').html('');
      $('#name').css('border-color', '');
      $('#razon').css('border-color', '');
      $('#message').css('border-color', '');
      $('#email').css('border-color', '');

      if (inputName.length!= 0 && inputAsunto.length!= 0 && inputMessage.length!= 0 && inputEmail.checkValidity() ) {
        $.ajax({
        url: "https://formspree.io/dose.prg.2017@gmail.com",
        method: "POST",
        data: {message: "From: " + mail + " Name: " + inputName + " Message: " + inputMessage,
               _subject: inputAsunto },
        dataType: "json"
        });
        $("#submitContact")[0].reset()
        $('#contactFormSuccess').addClass('alert alert-success').html('<strong>Perfect!</strong> Message sent correctly');
      }
      else {
        if (inputName.length== 0 || inputAsunto.length== 0 || inputMessage.length== 0 || !inputEmail.checkValidity()) {
          if(inputName.length== 0)
            $('#name').val('').css( "border-color", "red" );
          if(inputAsunto.length== 0)
            $('#razon').val('').css( "border-color", "red" );
          if(inputMessage.length== 0)
            $('#message').val('').css( "border-color", "red" );
          if(!inputEmail.checkValidity())
            $('#email').val('').css( "border-color", "red" );
          $('#contactFormError').addClass('alert alert-danger').html('<strong>Error!</strong> Corrects the fields marked in red.');
        }
      }
    })
  }
