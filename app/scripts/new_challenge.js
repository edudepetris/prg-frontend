import page from 'page'
import codeMirror from './helpers/codemirror.helper'
import api from './config/api'

  let url = '/challenge';
  var type;

  export default function (ctx,next) {
    type = ctx.params.type;
    var codeM;
    var codeTest;
    const html = PRG.templates.new_challenge();

    switch(type){
      case "compile":
        document.querySelector('#content').innerHTML = html
        document.querySelector('#title').innerHTML = "New Challenge: <i>Compile</i>";        
        codeM = codeMirror('#code');
        break;
      case "test":     
        document.querySelector('#content').innerHTML = html
        document.querySelector('#title').innerHTML = "New Challenge: <i>Test Passing</i>";
        document.querySelector('#test_p').innerHTML = "Tests Area";        
        document.querySelector('#tests_area').style.display=null;
        $('#code').html(PRG.templates.testProgramTemplate())
        codeM = codeMirror('#code');
        codeTest = codeMirror('#test');
        break;
      case "verification":
        document.querySelector('#content').innerHTML = html
        document.querySelector('#title').innerHTML = "New Challenge: <i>Verification</i>";
        document.querySelector('#test_p').innerHTML = "Verification Area";        
        document.querySelector('#tests_area').style.display=null;
        $('#test').html(PRG.templates.verificationProgramTemplate())
        $('#code').html(PRG.templates.verificationCodeTemplate())
        codeM = codeMirror('#code');
        codeTest = codeMirror('#test')
        break;
      default:
        document.querySelector('#content').innerHTML = html
        $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> Undefined.');
        break;
    }

    let button = $('#submitData');

    button.click(function() {
      var inputDescription = document.getElementById('description').value;
      var inputCode = codeM.getValue();
      var testInput;
      let verification;
      if (type=='verification') {
        verification = codeTest.getValue();
      }
      if(type=="test"){
        testInput = codeTest.getValue();
      }
      var inputNameChallenge = document.getElementById('nameChallenge').value;
      var inputDiffic = parseInt(document.getElementById('select_dif').value);
      var validDiff = inputDiffic>=0 && inputDiffic<5

      $('#nameChallenge').css('border-color', '');
      $('#description').css('border-color', '');

      $('#new_challengeError').removeClass('alert alert-danger').html('');

      switch(type){
        case "compile":
          sendData(inputDescription,inputCode,inputNameChallenge,inputDiffic);
          break;
        case "test":
          sendData(inputDescription,inputCode,inputNameChallenge,inputDiffic,testInput);
          break;
        case "verification":
          sendData(inputDescription,inputCode,inputNameChallenge,inputDiffic,null,verification);
          break;
        default:
          $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> Undefined.');
          break;
      }
    })

    function sendData(inputDescription, inputCode, inputNameChallenge, inputDiffic, testInput, verification){
      var validDiff = inputDiffic>=0 && inputDiffic<5

      if (inputDescription != "" && inputCode != "" && inputNameChallenge != "" && validDiff) {
        if(type == 'compile'){
          api.post(url, {
            name: inputNameChallenge,
            difficulty: inputDiffic,
            description: inputDescription,
            program: inputCode
          })
          .then(function (response) {
            if (response.status == 200) {
              page.redirect('/');
            }
          })
          .catch(function (error) {
            $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> ' + error.response.data.error);
          });
        }
        else{
          if (type == 'test') {
            if(testInput != ""){
              api.post(url, {
                name: inputNameChallenge,
                difficulty: inputDiffic,
                description: inputDescription,
                program: inputCode,
                test: testInput
              })
              .then(function (response) {
                if (response.status == 200) {
                  page.redirect('/');
                }
              })
              .catch(function (error) {
                $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> ' + error.response.data.error);
              });
            }
            else{
              $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> Write any tests.');
            }
          }
          else{
            if(verification != ""){
              api.post(url, {
                name: inputNameChallenge,
                difficulty: inputDiffic,
                description: inputDescription,
                program: inputCode,
                verification: verification
              })
              .then(function (response) {
                page.redirect('/');
              })
              .catch(function (error) {
                $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> ' + error.response.data.error);
              });
            }
            else{
              $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> Write a verification program.');
            }
          }
        }
      }
      else {
        if (inputDescription == "" || inputCode == "" || inputNameChallenge == "" ) {
          if(inputDescription == "")
            $('#description').val('').css( "border-color", "red" );
          if(inputNameChallenge == "")
            $('#nameChallenge').val('').css( "border-color", "red" );
          $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> Corrects the fields marked in red.');
          if(inputCode == "")
            $('#new_challengeError').addClass('alert alert-danger').html('<strong>Error!</strong> Write code source.');

        }
      }
    }
  }