import page from 'page'
import api from './config/api'
import * as session from './config/session'

let url = '/sign_in';

export default function () {
  const html = PRG.templates.signIn()
  document.querySelector('#content').innerHTML = html

  let button = $('#loginBtn');
  let email = $('#inputEmail');
  let pass = $('#inputPassword');

  button.click(function(event) {
    event.preventDefault();
    var inputEmail = document.getElementById('inputEmail');
    var inputPassword = document.getElementById('inputPassword');
    var remember=$('#remember-me')[0].checked;
    if (inputEmail.checkValidity() && inputPassword.checkValidity()) {

      $('#loginError').removeClass('alert alert-danger').html('');

      api.post(url, {
      email: email.val(),
      password: pass.val()
      })
      .then(function (response) {
        if (response.status == 200) {
          session.signin(response.data,remember);
          page.redirect('/');
        }
      })
      .catch(function (error) {
        $('#loginError').addClass('alert alert-danger no-margin-bottom error-alert-fit').html('<strong>Error!</strong> Invalid Email or Password.');
      });
    }
    else {
      if (inputEmail.checkValidity() == false) {
        $('#loginError').addClass('alert alert-danger no-margin-bottom error-alert-fit').html('<strong>Error!</strong> Invalid Email.');
      }
      else if (inputPassword.checkValidity() == false) {
        $('#loginError').addClass('alert alert-danger no-margin-bottom error-alert-fit').html('<strong>Error!</strong> Invalid Password.');
      }
    }
  })
}
