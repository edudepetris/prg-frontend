import viewUserProfile from './viewUserProfile'
import page from 'page'
import * as session from './config/session'
import api from './config/api'

export default function (ctx, next) {

  let url = ctx.path;
  let array = url.split('/');
  if (array.length > 2) {
    let username = array[2];
    viewUserProfile(username);
  }
  else {
    let profileLabels = ['usernameLabel', 'countryLabel'];
    let profileDivs = ['usernameDiv', 'countryDiv'];

    api.get('/user')
    .then(res => {
      if (!res.data.imageURL) {
        res.data.imageURL='../images/profile/defaultProfile.jpg'
      }
      const html = PRG.templates.profile(res.data)
      document.querySelector('#content').innerHTML = html
      // hide missing data
      for (var i = 0; i < profileLabels.length; i++) {
        var elem = $('#'+profileLabels[i]);
        var elemContainer = $('#'+profileDivs[i]);
        if (!(elem.html()).replace(/\s/g, '').length) {
          elemContainer.hide()
        }
        $('#edit-avatar').hide();
      }
      // Actions to execute when the info tab is clicked
      $('#infoBtn').click(function(e) {
        e.preventDefault();
        window.history.pushState("", "", '/profile#info');
        $('#ed').css("background-color","transparent");
        $('#stg').css("background-color","transparent");
        $('#inf').css("background-color","#ededed");
        $('#edit_profile').hide();
        $('#change_password').hide();
        $('#edit-avatar').hide();
        $('#userInfo').show();
        $("#profileImage").unbind('click');
        $('#profileImage').css('cursor','default');
      })
      // Actions to execute when the settings tab is clicked
      $('#settingsBtn').click(function(e) {
        e.preventDefault();
        window.history.pushState("", "", '/profile#settings');
        $('#inf').css("background-color","transparent");
        $('#ed').css("background-color","transparent");
        $('#stg').css("background-color","#ededed");
        $('#successMsg').hide();
        $('#errorMsg').hide();
        $('#input_password').val("");
        $('#input_password2').val("");
        $('#input_currentP').val("");
        $('#edit_profile').hide();
        $('#edit-avatar').hide();
        $('#userInfo').hide();
        $('#change_password').show();
        $("#profileImage").unbind('click');
        $('#profileImage').css('cursor','default');
      })
      // Actions to execute when the edit tab is clicked
      $('#editBtn').click(function(event) {
        event.preventDefault();
        window.history.pushState("", "", '/profile#edit');
        $('#inf').css("background-color","transparent");
        $('#stg').css("background-color","transparent");
        $('#ed').css("background-color","#ededed");
        $('#userInfo').hide();
        $('#change_password').hide();
        $('#edit_profile').show();
        $('[name="countriesL"] option:contains('+res.data.country+')').prop('selected',true);
        $('[name="countriesL"]').select2();
        $('#edit-avatar').show();
        $('#profileImage').css('cursor','pointer');
        $("#profileImage").click(function(e) {
          $("#imageUpload").click();
        })
      })
      // Actions to execute when the user wants to change his password
      $('#updatePassword').click(function() {
        var password = $('#input_password').val();
        var password2 = $('#input_password2').val();
        if (password == password2) {
          api.post('/change_password', {
            currentPassword: $('#input_currentP').val(),
            password: password,
            password2: password2
          })
          .then(function (response) {
            if (response.status == 200) {
              $('#errorMsg').hide();
              $('#successMsg').show();
              $('#successMsg').html('<strong>Success!</strong> Your password has been updated.');
            }
          })
          .catch(function (error) {
            $('#successMsg').hide();
            $('#errorMsg').show();
            $('#errorMsg').html('<strong>Error!</strong>'+error);
          });
        }
        else {
          $('#successMsg').hide();
          $('#errorMsg').show();
          $('#errorMsg').html('<strong>Error!</strong> Passwords does not match.');
        }
      })

      $('#saveChanges').click(function(e) {
        var cl = $('#countriesList')[0];
        var country = cl.options[cl.selectedIndex].text;
        api.post('/save_profile', {
          name: $('#input_name').val(),
          username: $('#input_username').val(),
          country: country,
          occupation: $('#input_occupation').val(),
          imageURL: $('#profileImage').attr('src')
        })
        .then(function (response) {
          if (response.status == 200) {
            $('body>#content').prepend('<span class="fade in alert alert-success" style="width: 100%; display: block;text-align: center;position: fixed; z-index: 1000; top: 288px; box-shadow: 0 0 43px;">Su perfil fue exitosamente actualizado, ahora debera volver a iniciar sesion.</span>')
            setTimeout(() => {
              session.signout()
              page.redirect('/sign_in');
            }, 3000)
          }
        })
        .catch(function (error) {
          $('#edit_error').html('<strong>Error! </strong>'+error);
        });
      })

      function fasterPreview( uploader ) {
        if ( uploader.files && uploader.files[0] ) {
          $('#profileImage').attr('src', window.URL.createObjectURL(uploader.files[0]) );

          // Begin file upload

          // Replace ctrlq with your own API key
          var apiUrl = 'https://api.imgur.com/3/image';
          var apiKey = 'a83c5aa00bfa978';
          var anonymousKey = '80a490d27ef53a37d51d878a1293b746074c3e40';

          var settings = {
            async: false,
            crossDomain: true,
            processData: false,
            contentType: false,
            type: 'POST',
            url: apiUrl,
            headers: {
              Authorization: 'Client-ID ' + apiKey,
              Accept: 'application/json'
            },
            mimeType: 'multipart/form-data'
          };

          var formData = new FormData();
          formData.append("image", uploader.files[0]);
          settings.data = formData;

          // Response contains stringified JSON
          // Image URL available at response.data.link
          $.ajax(settings).done(function(response) {
            let link = JSON.parse(response).data.link;
            $('#imageURL').val(link)
            $('#profileImage').attr("src", link);
          });
        }
      }

      $("#imageUpload").change(function() {
        fasterPreview( this );
      })

      if (location.hash == '#edit') {
        $('#editBtn').click();
      }
      else if (location.hash == '#info'){
        $('#infoBtn').click();
      }
      else if (location.hash == '#settings') {
        $('#settingsBtn').click();
      }
    })
    .catch(function (error) {
      document.querySelector('#content').innerHTML = 'no fetch data'
    })
  }
}
