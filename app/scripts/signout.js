import page from 'page'
import * as session from './config/session'
import api from './config/api'

const url_signout = '/sign_out'

export default function () {
  api.get(url_signout)
  .then(response => {
    if (response.status == 200) {
      session.signout();
      page.redirect('/');
    }
  })
  .catch(error => {
  });
}
