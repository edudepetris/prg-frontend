import page from 'page'
import landing from './landing'
import home from './home'
import dashboard from './dashboard'
import myChallenges from './myChallenges'
import signup from './signup'
import signIn from './signIn'
import notfound from './notfound'
import navigationbar from './navigationbar'
import signout from './signout'
import new_challenge from './new_challenge'
import about from './about'
import play from './play'
import contact from './contact'
import profile from './profile'
import help	from './help'

page('/*', navigationbar)
page('/', home)
page('/landing', landing)
page('/sign_in', signIn)
page('/dashboard', dashboard)
page('/myChallenges/user', myChallenges)
page('/help', help)
page('/signup', signup)
page('/signout', signout)
page('/create/:type', new_challenge)
page('/about', about)
page('/new/game/:id', play)
page('/contact', contact)
page('/profile', profile)
page('/profile/:username',profile)
page('*', notfound)
page()
