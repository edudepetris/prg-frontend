import page from 'page'
import api from './config/api'
import * as session from './config/session'
import {set_type_my,set_levels_my} from './helpers/myChallenges.helper'

export default function () {
  //full-width container
  var container = document.querySelector('body > div.container');
  if (container) {
    container.classList.remove('container');
    container.classList.add('container-fluid');
  }

    //full height content
  container = $('#content')[0];
  container.style.minHeight=(window.innerHeight-document.body.clientHeight)+$('#content').height()+'px';

  //get challenges
  api.get('/myChallenges/user')
  .then(data => {
    set_levels_my(data.data);
    set_type_my(data.data);
    const html = PRG.templates.myChallenges(data)
    document.querySelector('#content').innerHTML = html
    //assign functionality to selectors
    document.querySelectorAll('select.filter').forEach(function (sel) {
      sel.addEventListener("change",filter_mychallenges);
    });
    if (data.data.challenges === undefined || data.data.challenges.length == 0) filter_mychallenges();
  })
  .catch(function (error) {
    if (error.response) {
      if (error.response.status==500) {
        session.signout();
        page.redirect('/');
      }
      $('#content').html(error.response.data);
    }
    else
      document.querySelector('#content').innerHTML = 'no fetch data'
  })
}

/*
* filter challenges items by type and difficulty criteria
*/
function filter_mychallenges() {
  var diff = document.getElementById('select-diff').value;
  var type = document.getElementById('select-type').value;
  var diff_f = (diff=='0')? '' : '[data-diff="'+diff+'"]';
  var type_f = (type=='0')? '' : '[data-type="'+type+'"]';
  var crit=diff_f+type_f;
  $('#empty-alert')[0].style.display='none';
  document.querySelectorAll('.chall-li').forEach(function(elm){
    elm.style.display=null;
  })
  if (crit != '') {
    $('.chall-li:not('+crit+')').get().forEach(function(elm){
      elm.style.display='none';
    })
  }
  if ($('.chall-li:not([style*="display"])').length==0) {
  $('#empty-alert')[0].style.display=null;

  }
}
