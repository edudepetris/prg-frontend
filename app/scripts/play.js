import api from './config/api'
import codeMirror from './helpers/codemirror.helper'
import submissions from './play/submissions'
import ranking from './play/ranking'

var g_id;
export default function (ctx,next) {
    //challenge id in ctx.params.id
  let chall_id = ctx.params.id
  api.get('/new/game/'+chall_id)
  .then(res => {
    res.data.description=res.data.description.replace(/\n/g,'<br>');
    const html = PRG.templates.play(res.data)
    document.querySelector('#content').innerHTML = html
    g_id=res.data.id;

    var cm = codeMirror('#editor-ta');

    $('#save-btn' ).on('click',() => {
      save_game(cm);
    });
    $('#check-btn').on('click',() => {
      check_game(cm);
    });

    //Hide-Show panels
    $(document).on('click', '.panel-heading span.clickable', function (e) {
      var $this = $(this);
      if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
      } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
      }
    });

    //Show submissions
    $('a[href="#subs-tab"]').on('click',ev =>{submissions(chall_id)})

    //Show ranking
    $('a[href="#ranking-tab"]').on('click',ev =>{ranking(chall_id)})
  })
  .catch(function (error) {
    document.querySelector('#content').innerHTML = error.message;
  })

}

function save_game(cm) {
  api.post('/game/save',{
    'game_id': g_id,
    'code'   : cm.getValue()
  })
  .then(res => {
    var text = '<strong>Done!</strong> <span> The current game has been saved.</span>';
    show_alert(text,'info');
  })
  .catch(error => {
  });
}

function check_game(cm) {
  //First save
  api.post('/game/save',{
    'game_id': g_id,
    'code'   : cm.getValue()
  })
  .then(ok => {
    //Then check
    api.get('/game/'+g_id+'/check')
    .then(res => {
      var msg = '<strong>Great!</strong> <span> You have solved the problem. Your score is </span><strong>'+res.data.score+'</strong>';
      var type = 'success';
      if (typeof res.data.was_ok == 'string') {
        res.data.was_ok = eval(res.data.was_ok);
      }
      if (res.data.score && typeof res.data.score == 'string') {
        res.data.score = parseInt(res.data.score);
      }
      if (!res.data.was_ok) {
        msg = '<strong>Ouch!</strong> <span> It seems something is wrong with your code</span>';
        type = 'danger';
      }
      show_alert(msg,type);
      $('#console samp').html(res.data.result.replace(/\n/g,'<br>'));
    })
    .catch(error => {
    });
  })
  .catch(error => {
  });
}

function show_alert(html,type) {
  var alert = $('#alert-msg')[0];
  var alertcont = alert.parentElement;
    alert.classList.add('alert-'+type);
    alert.innerHTML=html;
    //unfold message
    alertcont.style.maxHeight = (alert.clientHeight+2)+'px';
    //wait to close the message
    setTimeout(function () {
      alertcont.style.maxHeight = null;
      setTimeout(function () {
        alert.classList.remove('alert-'+type);
      },1000)
    }, 5000);
}
