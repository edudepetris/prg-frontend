import page from 'page'
import * as session from './config/session'

export default function() {

  let target = window.location.href;
  let indexOfQuestionMark = target.indexOf('?');

  if (indexOfQuestionMark == -1) {
    if (session.isOpen()) {
      page.redirect('/dashboard');
    }
    else {
      page.redirect('/landing');
    }
  }
  else {
    target = target.substring(indexOfQuestionMark + 1);
    page.redirect('/'+target);
  }
}
