// axios default configuration base on environment.
//
// example.js
// import api from './config/api'
// api.get('/challenges') # =>
//   GET //prg.mockable.io/challenges

import axios from 'axios'
import * as session from './session'

let protocol = 'location.protocol';
let hostname = 'localhost'
if (typeof location !== 'undefined'){
  protocol = location.protocol
  hostname = location.hostname
}

export default axios.create({
  // baseURL: protocol+'//'+'prg.zapto.org',
  baseURL: protocol+'//'+'prg.mockable.io',
  //baseURL: protocol+'//'+hostname+':4567',
  transformRequest: [function (data, headers) {
    // Show spin modal
    $('#load-modal div')[0].style.display='block';

    //don't send auth header on login/signup
    if (data===undefined || data.password === undefined) {
      headers.common['Authorization'] = session.token();
    }

    headers.post['Content-Type'] = 'application/json'
    return JSON.stringify(data);
  }],
  transformResponse: [function (data) {
    // Hide spin modal
    $('#load-modal div')[0].style.display=null;

    var res;
    try {
      res = JSON.parse(data)
    } catch(e) {
      //it's no a json
      res = data;
    }
    return res;
  }]
})


function setAuthHeader(argument) {
  axios.defaults.headers.common['Authorization'] = session.token();
}

function resetAuthHeader(argument) {
  delete axios.defaults.headers.common['Authorization'];
}

export {setAuthHeader, resetAuthHeader}

// Alter defaults after instance has been created
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
