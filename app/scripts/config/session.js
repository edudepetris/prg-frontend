/*
* JS Module for session handling
  Usage:
    import * as session from './config/session'

    session.sigin();   // Open session
    session.signout(); // Close session
    session.isOpen();  // Check is session is open

*/

import * as api from './api'
global.atob = require('atob')

let storage = localStorage;

export function signin(res,remember) {
  var token = res.token;
  if (!remember) {
    storage=sessionStorage;
  }
  storage.prgsession = token;
  api.setAuthHeader();
}

export function signout() {
  storage.removeItem('prgsession');
  api.resetAuthHeader();
}

export function isOpen() {
  return storage.prgsession !== undefined;
}

export function getUserData() {
  if (isOpen()){
    var token = storage.prgsession;
    var data = token.split('.')[1];
    return JSON.parse(_atob(data));
  }
}

export function _atob (data) { return atob(data) }

export function token() {
  if (isOpen()) return storage.prgsession;
  return "";
}
