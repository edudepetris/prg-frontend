import sinon from 'sinon'
import { expect } from 'chai'
import * as session from '../../app/scripts/config/session'
import * as api from '../../app/scripts/config/api'
import axios from 'axios'

(function () {
  'use strict';

  describe('api module', function () {
    describe('setAuthHeader()', function () {

      let sessionMock
      
      beforeEach(function() {
        sessionMock = sinon.mock(session);
      })

      afterEach(function() {
        sessionMock.restore()
        global.localStorage.prgsession = undefined
      })
     
      it('set header whit token', function () {
        global.localStorage.prgsession = 'open session'
        api.setAuthHeader()
        expect(axios.defaults.headers.common['Authorization']).to.be.eq(session.token())

      });
    });

    describe('resetAuthHeader()', function () {

      let sessionMock
      
      beforeEach(function() {
        sessionMock = sinon.mock(session);
      })

      afterEach(function() {
        sessionMock.restore()
        global.localStorage.prgsession = undefined
      })
     
      it('set header whit token', function () {
        global.localStorage.prgsession = 'open session'
        api.resetAuthHeader()
        expect(axios.defaults.headers.common['Authorization']).to.be.undefined

      });
    });



  });
})();
