import sinon from 'sinon'
import { expect } from 'chai'
import * as session from '../../app/scripts/config/session'
import * as api from '../../app/scripts/config/api'

describe('Session.js', function () {

  describe('#isOpen', function () {

    it('returns false if it does not open', function () {
      expect(session.isOpen()).to.be.false
    })

    it('returns true if it is open', function () {
      global.localStorage.prgsession = 'open session' // set localStorage.
      expect(session.isOpen()).to.be.true
    })
  })

  describe('#signin', function () {
    const res = { "token": "abc" }

    it('sets authorization header', function () {
      const setAuthHeader = sinon.spy(api, 'setAuthHeader')
      session.signin(res, true)
      sinon.assert.calledOnce(setAuthHeader)
    })
  })

  describe('#signout', function () {
    const res = { "token": "abc" }

    it('reset authorization header', function () {
      const resetAuthHeader = sinon.spy(api, 'resetAuthHeader')
      session.signout()
      sinon.assert.calledOnce(resetAuthHeader)
    })
  })

  describe('#getUserData', () => {

    let sessionMock

    beforeEach(function() {
      sessionMock = sinon.mock(session);
    })

    afterEach(function() {
      sessionMock.restore()
      global.localStorage.prgsession = undefined
    })

    it('returns data from token', function() {
      global.localStorage.prgsession = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJuYW1lIjoidGVzdCJ9.NU-JwmJGX-KP9-JSnS01Li0-nEphHDabSjzkOr3ndEE'
      sessionMock.expects('isOpen').returns(true)
      // sessionMock.expects('_atob').returns('{ "email": "test@test.com", "name": "test" }')
      expect(session.getUserData()).to.deep.include({ "email": "test@test.com", "name": "test" })
    })
  })

  describe('#token', () => {
    let sessionMock

    beforeEach(function() {
      sessionMock = sinon.mock(session);
    })

    afterEach(function() {
      sessionMock.restore()
      global.localStorage.prgsession = undefined
    })

    it('returns saved token if session is open', () => {
      global.localStorage.prgsession = 'open session' // set session.
      sessionMock.expects('isOpen').returns(true);
      expect(session.token()).to.be.eq('open session')
    })

    it('returns empty string if session is not open', () => {
      sessionMock.expects('isOpen').returns(false);
      expect(session.token()).to.be.eq('')
    })
  })
})

